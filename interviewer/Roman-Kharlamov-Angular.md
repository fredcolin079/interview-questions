#Roman Kharlamov's tech interview

# Ответ подходит ✅
- [Типы позиционирования в CSS?](https://youtu.be/ycYp7CYOnO0?t=321) 
- [Разница между `display: none` и `visibility: hidden`?](https://youtu.be/1eIRTdgzHtw?t=139) [Common]
- [Что такое селектор? И какие селекторы существуют?](https://youtu.be/G7hLwudGWL4?t=282) 
- [Что такое CSS препроцессор?](https://youtu.be/kx3dR6ztICU?t=159) [Common]
- [Разница между `<script>`, `<script async>` ](https://youtu.be/rlWgI7AvV18?t=77) [Middle]
- [Что такое семантика? Какие семантичные тэги вы знаете?](https://youtu.be/ycYp7CYOnO0?t=149) [Common]
- [Расскажите о meta-теге с `name="viewport"`?](https://youtu.be/rWEsjNWBoIE?t=36) [Common]
- [Разница между адаптивным (adaptive) и отзывчивым (responsive) дизайнами?](https://youtu.be/1eIRTdgzHtw?t=233) [Common]
- [Что такое кроссбраузерность?](https://youtu.be/kx3dR6ztICU?t=90) [Common]
- [Что такое Babel? Для чего он используется?](https://youtu.be/w-vUj0gHGgg?t=680) [Middle]
- [Что такое ECMAScript? В чём отличие от JavaScript?](https://youtu.be/IooJ3P2VUYs?t=336) [Common]
- [Разница между `let`, `const` и `var`?](https://youtu.be/1eIRTdgzHtw?t=361) [Common]
- [Типы данных в JavaScript?](https://youtu.be/ycYp7CYOnO0?t=471) [Common]
- [Операторы (`&&` и `||` и `??`)?](https://youtu.be/G7hLwudGWL4?t=617) [Common]
    - statement1 && statement2
    - statement2 || statement2
    - statement2 ?? statement2
- [Что обозначает `this` в JavaScript?](https://youtu.be/rlWgI7AvV18?t=507) [Common/Middle]
- [Что такое замыкание (Closure)?](https://youtu.be/kx3dR6ztICU?t=284) [Common/Middle]
- hoisting
- [Разница между `.forEach` и `.map()`?](https://youtu.be/rlWgI7AvV18?t=456) [Common/Middle]
- [Что такое чистая функция?](https://youtu.be/rlWgI7AvV18?t=401) [Middle]
- [Что такое чистая функция?](https://youtu.be/rlWgI7AvV18?t=401) [Middle]
  ```javascript
    const a = (counter) => {
      console.log(counter); // do not side effect
      return counter + 1;
    };
    ```

# Ошибки или не точности
- Сравнение двух обьектов
- [Разница между `null` и `undefined`?](https://youtu.be/G7hLwudGWL4?t=511) [Common]
- [Что такое Progressive Web Application - Service Worker & Web Workers ?](https://youtu.be/XtQPrt8G0n8?t=76) [Middle]
- [Разница между Progressive Enhancement и Graceful Degradation?](https://youtu.be/rlWgI7AvV18?t=286) [Common]
- [Разница между `<script>`, `<script defer>` ](https://youtu.be/rlWgI7AvV18?t=77) [Middle]
- [Что такое мета-тэги?](https://youtu.be/ngyOYuTrUk8?t=205) [Middle]
- [Для чего используют `data-`атрибуты?](https://youtu.be/rlWgI7AvV18?t=34) [Common/Middle] 
